from fabric.contrib.files import exists
from fabric.api import env, local, run

REPO_URL = 'git@bitbucket.org:itsmagic/svsi_driver.git'

def deploy():
    site_folder = f'/home/{env.user}/drivers/svsi_driver'
    source_folder = site_folder + '/source'
    _create_directory_structure_if_necessary(site_folder)
    _get_latest_source(source_folder)
    _update_virtualenv(source_folder)
    _restart_service()

def _create_directory_structure_if_necessary(site_folder):
    for subfolder in ('virt_env', 'source'):
        run(f'mkdir -p {site_folder}/{subfolder}')

def _get_latest_source(source_folder):
    if exists(source_folder + '/.git'):
        run(f'cd {source_folder} && git fetch')
    else:
        run(f'git clone {REPO_URL} {source_folder}')
    current_commit = local("git log -n 1 --format=%H", capture=True)
    run(f'cd {source_folder} && git reset --hard {current_commit}')

def _update_virtualenv(source_folder):
    virtualenv_folder = source_folder + '/../virt_env'
    if not exists(virtualenv_folder + '/bin/pip'):
        run(f'python3.8 -m venv {virtualenv_folder}')
    run(f'{virtualenv_folder}/bin/pip install --upgrade pip')
    run(f'{virtualenv_folder}/bin/pip install --upgrade -r {source_folder}/requirements.txt')

def _restart_service():
    run("sudo systemctl restart svsi_driver.service")

