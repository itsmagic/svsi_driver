from threading import local
from driver_base import DriverBase, Device
from scripts import svsi_scanner, svsi_monitor, interface_monitor, svsi_unit_monitor, datastore, svsi_devices_queue

import signal
import time
import os
import sys
import logging
from netaddr import IPRange
import queue
from pathlib import Path
from pydispatch import dispatcher

logger = logging.getLogger("SVSiDriver")

class SVSiDriver(DriverBase):
    def __init__(self):
        self.svsi_devices = []
        self.driverconfig = []
        self.interface_list = []
        self.multicast_listeners = []
        self.devices_to_be_added = []
        self.monitor_threads = {}
        self.poll_count = 0
        self.ip_range = None

        # We need to launch the SVSi discovery at this point
        dispatcher.connect(self.discovered_new_unit, signal="Unit Detected")
        dispatcher.connect(self.interface_update, signal="Interface Update")
        dispatcher.connect(self.svsi_unit_update, signal="SVSI Unit Status")

        self.interface_queue = queue.Queue()
        ip_monitor = interface_monitor.InterfaceMonitor(self.interface_queue)
        ip_monitor.setDaemon(True)
        ip_monitor.start()

        self.svsi_devices_queue_thread = queue.Queue()
        update_queue = svsi_devices_queue.SVSiDevicesQueue(self.svsi_devices, self.svsi_devices_queue_thread)
        update_queue.setDaemon(True)
        update_queue.start()

        self.interface_queue.put(['check_changes'])

        super().__init__()

    def restart_multicast(self):
        """We need a thread to listen on every interface"""
        logger.info("Interface update so restarting multicast thread")
        for listener in self.multicast_listeners:
            # logger.error('stopping multicast')
            listener.shutdown = True
            listener.join()
        for interface in self.interface_list:
            multicast_listener = svsi_monitor.SVSiMonitor(ip_address=interface.ip_addresses[0])
            multicast_listener.setDaemon(True)
            multicast_listener.start()
            self.multicast_listeners.append(multicast_listener)

    def on_broadcast(self):
        my_interfaces = []
        for interface in self.interface_list:
            # logger.info(f"Sending broadcast on interface:{interface.index}")
            my_scan = svsi_scanner.SVSiScanner(interface=interface)
            my_interfaces.append(interface.description)
            my_scan.setDaemon(True)
            my_scan.start()

    def unit_broadcast(self, unit):
        my_interfaces = []
        for interface in self.interface_list:
            # logger.info(f"Sending broadcast on interface:{interface.index}")
            my_scan = svsi_scanner.SVSiScanner(interface=interface, ip_address=unit.ip_address)
            my_interfaces.append(interface.description)
            my_scan.setDaemon(True)
            my_scan.start()

    def interface_update(self, sender, interface_list):
        """Processes updates"""
        # logger.error('In interface update')
        self.interface_list = interface_list
        # logger.error('Starting multicast and sending broadcast')
        self.restart_multicast()
        self.on_broadcast()

    def on_updated_driverconfig(self, config):

        self.driverconfig = config

        need_to_add_broadcast = True
        for config in self.driverconfig:
            if config['unique_id'] == self.driverbaseconf.uuid:
                need_to_add_broadcast = False
            if 'ip_range' in config:
                    self.ip_range = config['ip_range']

        if need_to_add_broadcast:
            new_config = {'description': 'Broadcast Discovery',
                        'name': 'SVSi Broadcast Discovery',
                        'unique_id': self.driverbaseconf.uuid,
                        'talents': [{'name': 'SVSi Broadcast',
                                    'description': 'Sends Broadcast',
                                    'command': 'Discover'}]}
        
            self.driverconfig.append(new_config)
            self.update_driverconfig(self.driverconfig)

                    
    def svsi_unit_update(self, data):
        logger.info(f"Received an SVSi monitor update need to react to it {data}")
        web_device = self.get_device_by_unique_id(data['unit'].mac_address)
        if not web_device:
            logger.debug(f"Ignoring Update from unknown device {data['unit']}")
            return
        if 'online' in data:
            logger.info(f"Patching device {data['unit']} to online {data['online']}")
            self.device_update(device=web_device, online=data['online'])

        if 'stream' in data:
            if data['unit'].box_type == 'TX':
                web_videoinput = web_device.get_videoinput_by_name(data['unit'].mac_address)
                if not web_videoinput:
                    logger.debug(f"Ignoring Update from unknown videodevice {data['unit']}")
                    return
                if str(web_videoinput.number) != data['stream']:
                    self.videoinput_update(videoinput=web_videoinput, number=int(data['stream']))
            if data['unit'].box_type == 'RX':
                web_videooutput = web_device.get_videooutput_by_name(data['unit'].mac_address)
                if not web_videooutput:
                    logger.debug(f"Ignoring Update from unknown videodevice {data['unit']}")
                    return
                if str(web_videooutput.number) != data['stream']:
                    self.videooutput_update(videooutput=web_videooutput, number=int(data['stream']))

    def discovered_new_unit(self, data, multicast):
        """Unit discovered by multicast"""
        # We don't want to mark new_units_found if the unit already exists
        # logger.info(f"New unit: {data}")
        for unit in self.svsi_devices:
            if unit.mac_address == data.mac_address:
                # logger.info(f"We already have this unit {unit.mac_address}")
                # But we should still update IP address, just in case it has changed...
                unit.ip_address = data.ip_address
                return
        if multicast:
            self.unit_broadcast(unit=data)
        else:
            data.get_status_populate()
        self.svsi_devices.append(data)

    def on_videoroute_select(self, videoinput, videooutputs, talent):
        # On SVSi we just change the stream number on the decoders
        logging.debug(f"In videoswitch: {videoinput}, {videooutputs}, {talent}")
        if talent.set_on:
            for decoder in videooutputs:
                my_monitor, my_queue = self.monitor_threads[decoder.device.unique_id]
                logger.info(f"Switching unit {decoder.device.unique_id} to stream {videoinput.number}")
                my_queue.put(['command', 'vunmute'])
                my_queue.put(['command', f'set:{videoinput.number}'])
            self.talent_update(talent=talent, state=True)
        if talent.set_off:
            for decoder in videooutputs:
                if decoder.device.unique_id in self.monitor_threads:
                    my_monitor, my_queue = self.monitor_threads[decoder.device.unique_id]
                    logger.info(f"Switching unit {decoder.device.unique_id} to stream {videoinput.number}")
                    my_queue.put(['command', "vmute"])
            self.talent_update(talent=talent, state=False)

    def on_talent_change(self, talent):
        logging.critical(f"Got talent update, need to respond: {talent}")
        if talent.device.unique_id == self.driverbaseconf.uuid:
            if talent.set_on:
                if self.ip_range is not None:
                    try:
                        logger.info(f"Sending Broadcast to range {self.ip_range}")
                        start, finish = self.ip_range.split("-")
                        ip_range = IPRange(start, finish)
                        for unit in [datastore.SVSIUnit(ip_address=str(a)) for a in ip_range]:
                            self.unit_broadcast(unit)
                    except Exception as error:
                        logger.critical(f"Unable to parse range {self.ip_range} should be in the format '192.168.1.1-192.168.1.10'")
                    
                else:
                    logger.info(f"Sending Broadcast discovery -- this will not work in a docker environment -- please set ip_range in svsi driver config")
                    self.on_broadcast()
                self.talent_clear(talent=talent, clear='set_on', in_transition=False)
            if talent.set_off:
                self.talent_clear(talent=talent, clear='set_off', in_transition=False)

        else:
            if talent.set_on:
                self.talent_clear(talent=talent, clear='set_on', in_transition=True)
                self.select_route(talent)
            if talent.set_off:
                self.talent_clear(talent=talent, clear='set_off', in_transition=True)
                self.select_route(talent)

    
    def periodic(self):
        # Populate interface list
        while not self.shutdown and self.interface_list == []:
            logger.info('Waiting for interfaces to be populated...')
            self.interface_queue.put(['send_update'])
            time.sleep(10)
            continue

        local_device = self.get_device_by_unique_id(self.driverbaseconf.uuid)
        if local_device:
            if local_device.online != True:
                self.device_update(device=local_device, online=True)

        for unit in self.svsi_devices:
            local_device = self.get_device_by_unique_id(unit.mac_address)
            if local_device:
                if local_device.online != unit.online:
                    # update unit
                    logging.debug(f"Found online out of sync, updating {local_device.id} online to {unit.online}")
                    self.device_update(device=local_device, online=unit.online)
                if unit.box_type == 'TX':
                    web_videoinput = local_device.get_videoinput_by_name(unit.mac_address)
                    if web_videoinput.number != int(unit.stream):
                        self.videoinput_update(videoinput=web_videoinput, number=unit.stream)
                if unit.box_type == 'RX':
                    web_videooutput = local_device.get_videooutput_by_name(unit.mac_address)
                    if web_videooutput.number != int(unit.stream):
                        self.videooutput_update(videooutput=web_videooutput, number=int(unit.stream))

            if unit.mac_address not in self.monitor_threads:
                # Start thread to monitor unit
                my_queue = queue.Queue()
                my_monitor = svsi_unit_monitor.SVSiUnitMonitor(unit=unit, job_queue=my_queue)
                my_monitor.daemon = True
                my_monitor.start()
                self.monitor_threads[unit.mac_address] = (my_monitor, my_queue)

                continue

        # logger.debug(f"Driver Config: {self.driverconfig}")
        units_to_be_added = []
        for unit in self.svsi_devices:
            local_device = self.get_device_by_unique_id(unit.mac_address)
            if not local_device:
                # We need to add
                if unit.name == "":
                    # This means we haven't gotten an update from the unit yet...
                    self.unit_broadcast(unit=unit)
                    continue
                else:
                    # we need to update the driverconfig
                    # but only if we have the unit online -- this way we can delete units
                    if unit.online:
                        units_to_be_added.append(unit)
        for unit in units_to_be_added:
            if unit.box_type == 'TX':
                description = f"Input -- {unit.model} -- {unit.mac_address}"
                new_config = {'description': unit.model,
                              'name': unit.name,
                              'unique_id': unit.mac_address,
                              'videoinputs': [{'name': unit.mac_address,
                                               'description': description}]}
            if unit.box_type == 'RX':
                description = f"Output -- {unit.model} -- {unit.mac_address}"
                new_config = {'description': unit.model,
                              'name': unit.name,
                              'unique_id': unit.mac_address,
                             'videooutputs': [{'name': unit.mac_address,
                                            'description': description}]}
            self.driverconfig.append(new_config)
        if units_to_be_added:
            self.update_driverconfig(self.driverconfig)




class ServerExit(Exception):
    pass


def service_shutdown(signum, frame):
    print('Caught signal %d' % signum)
    raise ServerExit


def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)

    my_driver = SVSiDriver()
    my_driver.daemon = True
    my_driver.start()
    try:
        while True:
            time.sleep(1)
    except ServerExit:
        logger.critical('Shutting down')
        dispatcher.send(signal="Shutdown")
        my_driver.join()


if __name__ == '__main__':
    main()
