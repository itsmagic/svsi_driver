# asyncio_echo_client_coroutine.py
import asyncio
import logging
import sys
import websockets
import json
import time


async def readws(ws, queue):
    while True:
        try:
            data = await ws.recv()
            await queue.put(data)
        except websockets.error:
            return None


async def writews(ws, queue):
    while True:
        data = await queue.get()
        ws.send(data)


async def worker_ws(ws, queue):
    print('in worker')
    while True:
        await queue.put({'stream': 'talentstream', 'payload': {"action": 'subscribe_instance', "request_id": 42, "pk": 7}})
        time.sleep(1)


async def websocket_monitor():
    s_queue = asyncio.Queue()
    r_queue = asyncio.Queue()
    # spawn two workers in parallel, and have them send
    # data to our queue
    async with websockets.connect('ws://localhost:8000/ws/multi/') as ws:
        ws_task = asyncio.create_task(readws(ws, r_queue))
        ws_send = asyncio.create_task(writews(ws, s_queue))
        ws_work = asyncio.create_task(worker_ws(ws, s_queue))
        # data = json.dumps({'stream': 'talentstream', 'payload': {"action": 'subscribe_instance', "request_id": 42, "pk": 7}})

        # await ws.send(data)
        # udp_task = asyncio.create_task(sendws(ws, queue))
        print('before loop')

        while True:
            data = await r_queue.get()
            print('from websocket', data)
            

        # terminate the workers
        ws_task.cancel()
        ws_send.cancel()
        ws_work.cancel()


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(websocket_monitor())
