import sys
import asyncio
import logging
import wmi
import datastore
import socket
import binascii
import re
import datetime
# from websocket import WebSocketApp
from reconnecting_websocket import ReconnectingWebsocket
import json
# from collections import defaultdict
from pydispatch import dispatcher
from threading import Thread
# Broadcast scan on demand
# Multicast join 
# interface monitor
# logging

INTERFACES = []
DISCOVERED_SVSI = []
SVSI_UNIT_QUEUES = {}
ws_send_queue = asyncio.Queue()

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("my_test_prog")
logging.getLogger("chardet.charsetprober").disabled = True


async def broadcast_scan_for_svsi():
    while True:
        data_packs = ['0114', '0119', '0196', '0198', '01b4', '01b5', '01b6',
                      '01b7', '01b8', '01b9', '01ba', '01ca', '01cb', '01cc',
                      '01cd', '01ce']
        data_tail = 'cafe1234ffffffff'
        for interface in get_current_interfaces():
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                sock.bind((interface.ip_addresses[0], 0))
                sock.setblocking(0)
            except IOError:
                # something has already used the port, its probably not us because
                # multiple instances can run without error due to SO_REUSEADDR
                logger.error('Socket in use')
                continue
        try:
            for data in data_packs:
                if data == '01b7':
                    sock.sendto(binascii.unhexlify(data + data_tail), ("255.255.255.255", 50006))
                else:
                    sock.sendto(binascii.unhexlify(data + data_tail), ("255.255.255.255", 50001))
                await asyncio.sleep(.1)
        except IOError as error:
            if error.args[0] == 10051:
                logger.error(error)
            else:
                logger.error(error)
                # return?
        count = 0
        timeout = 15
        waiting = True
        while waiting:
            count += 1
            if count > timeout:
                break
            try:
                # print('waiting')
                data, source = sock.recvfrom(4000)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    await asyncio.sleep(1)
                    continue
                else:
                    # Connection was probably closed
                    break
                    # print("Error listening: ", error)
            try:
                if data == '' or data[:4].decode() != 'SVSI':
                    # print('not equal: ', data)
                    continue
                serial_number = data.decode().split(':')[1].replace('NAME', '')
                reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                my_dict = {}
                for item in re.findall(reg_pat, data.decode()):
                    my_dict[item[0]] = item[1]
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=serial_number,
                                          # status="broadcast",
                                          get_status=my_dict)
            new_unit.get_status_populate()
            # Found units we need to create a queue and connect to them
            if new_unit not in DISCOVERED_SVSI:
                # logger.error(f'Found new unit {new_unit.mac_address}')
                DISCOVERED_SVSI.append(new_unit)
                await process_new_unit()
            # else:
            #     # logger.error(f'Found old unit {new_unit.mac_address}')
        # logger.error('Scan complete')
        await asyncio.sleep(10)


async def multicast_monitor_launcher():
    my_ips = []
    for interface in get_current_interfaces():
        my_ips.append(interface.ip_addresses[0])

    await asyncio.gather(*(multicast_monitor_for_svsi(ip) for ip in my_ips))


async def multicast_monitor_for_svsi(ip_address):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            port = 50019
            multicast_address = "239.254.12.16"
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((ip_address, port))
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(ip_address))

            sock.setblocking(0)

        except IOError as error:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            if error.args[0] == 10049:
                print("Skipping this error: ", error)
            else:
                print('Opening socket error: ', error)
                # Maybe we should warn the user?
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        data = ""
        while True:
            try:
                # print('waiting')
                data, source = sock.recvfrom(1024)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    await asyncio.sleep(1)
                    continue
                else:
                    print("Error listening: ", error)
            # check if it is a discovery message
            try:
                if data[:14].decode() != 'SVSI-Discovery':
                    print('not equal: ', data[:14].decode())
                    continue
                mac_address = ':'.join(['%02x' % item for item in data[16:22]])
                unit = datastore.SVSIUnit(arrival_time=datetime.datetime.now(),
                                          serial_number=data[30:44].decode(),
                                          name=data[50:67].decode(),
                                          mac_address=mac_address,
                                          ip_address=source[0],
                                          raw_multicast=data)
                logger.error(unit)
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            # dispatcher.send(signal="Unit Detected", sender=self, data=unit, multicast=True)
    except asyncio.CancelledError:
        logger.error('In cancelled error')
        # try:
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(ip_address))
        # print('leave: ', self.ip_address)
        sock.close()
        # print('socket closed')
        # 
        raise
    finally:
        logger.error('After cancelled')

async def process_new_unit():
    pass
    # logger.error(f'In process_new_units')
    # logger.error(DISCOVERED_SVSI)


async def ensure_registered(my_uuid, my_id):
    logger.error('in ensure')
    if my_uuid is None:
        await ws_send(stream='driverstream', payload={'action': 'create',
                                                'data': {'owner': {'id': my_id},
                                                         'name': 'NewSVSiDriver',
                                                         'devices': [],
                                                         'configs': []},
                                                'request_id': 42})



def ws_send(stream, payload):
    logger.error('sending')
    dispatcher.send("Websocket Send", stream=stream, payload=payload)

# async def my_send_action(ws):
#     ws.send(json.dumps({'stream': 'driverstream', 'payload': {"action": "retrieve", "request_id": 42, "pk": 7}}))

# def on_open(ws):
#     logger.error('on open')
#     # Now that we have the ws ... we can check the queue and send stuff
#     # i, ws_data = ws_send_queue.get()
#     # ws.send(json.dumps(ws_data))
#     # ws_send_queue.task_done()
    




# async def on_message(ws, msg):
#     logger.error(f'on msg: {msg}')
#     await queue.put(msg)


# def on_error(ws, error):
#     logger.error('on error')


# def on_close(ws):
#     logger.error('on close')





# async def connect_to_websocket(server_protocol='ws', server_uri='localhost', server_port=8000, reconnect_interval=5, token='ea3f95faff937c7b1aec017aae8b6b2a68754a4b'):
#     # on_open = lambda ws: self.ws_open(ws)  # noqa E731
#     # on_message = lambda ws, msg: self.ws_message(ws, msg)  # noqa E731
#     # on_error = lambda ws, error: self.ws_error(ws, error)  # noqa E731
#     # on_close = lambda ws: self.ws_close(ws)  # noqa E731
#     url = f'{server_protocol}://{server_uri}:{server_port}/ws/multi/'
#     header = [f'Authorization: Token {token}']
#     ws = WebSocketApp(url, header, on_open, on_message, on_error, on_close)
#     # await ws_send_queue.put({'stream': 'driverstream', 'payload': {"action": "retrieve", "request_id": 42, "pk": 7}})
#     # await ws.run_forever()
#     # logger.error('past forever')
#     return ws




async def connect_to_unit(unit, q):
    logger.error(f"Getting unit status {unit.mac_address}")
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((unit.ip_address, 50001))
        s.send(b'getStatus')
        data = s.recv(4096)
        if data[:4].decode() != 'SVSI':
            print('not equal: ', data)
            s.close()
            return
        serial_number = data.decode().split(':')[1].replace('NAME', '')
        reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
        my_dict = {}
        for item in re.findall(reg_pat, data.decode()):
            my_dict[item[0]] = item[1]
        unit.serial_number = serial_number
        unit.get_status = my_dict
        unit.get_status_populate()
        logger.error(f"Got unit status{unit.name}")
        while True:
            action = await q.get()
            logger.error('Got action')
            logger.error(action)


        # s.close()
    except Exception as error:
        logger.error(f"Getting unit status error {error}")
        # unit.status = f'Unable to get status: {error}'
        # dispatcher.send(signal='Status Update', data=unit)
        # print("Error getting status", error)
        return
    # unit.status = 'Success'

    # dispatcher.send(signal='Status Update', data=unit)


def get_current_interfaces():
    interfaces = []
    # logger.error('starting update update_current_interfaces')
    for interface in wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True):
        new_interface = datastore.InterfaceConfig(index=interface.Index,
                                                  description=interface.Description,
                                                  ip_addresses=list(interface.IPAddress),
                                                  subnet_addresses=list(interface.IPSubnet),
                                                  gateway_addresses=interface.DefaultIPGateway,
                                                  dhcp_enabled=interface.DHCPEnabled)

        new_interface.populate_broadcast_addresses()
        interfaces.append(new_interface)
    return interfaces
    # await asyncio.sleep(5)


# async def print_current_interfaces():
#     while True:
#         logger.error(msg=INTERFACES)
#         await asyncio.sleep(5)

# async def my_ws():
#     ws = Thread(target=ReconnectingWebsocket, kwargs={'token': 'ea3f95faff937c7b1aec017aae8b6b2a68754a4b'}).start()

def incoming(message):
    print(message)


async def main():
    # q = asyncio.Queue()
    # poll = await poll_current_interfaces()
    # my_print = await print_current_interfaces()
    my_uuid = None
    my_id = 7
    dispatcher.connect(incoming, signal="Websocket Incoming")
    try:
        # await asyncio.gather(multicast_monitor_launcher(), , broadcast_scan_for_svsi(), return_exceptions=True)
        ws = Thread(target=ReconnectingWebsocket, kwargs={'token': 'ea3f95faff937c7b1aec017aae8b6b2a68754a4b'})
        ws.daemon = True
        ws.start()
        # ws = 
        # await asyncio.run_until_complete(ws)
        await asyncio.gather(broadcast_scan_for_svsi(), ensure_registered(my_uuid, my_id), return_exceptions=True)
    except asyncio.CancelledError:
        print("Gracefully shutting down main")
        raise
    finally:
        print('Shutdown main')


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
    finally:
        print('Shutdown')

# loop = asyncio.get_event_loop()
# task = loop.create_task(poll_current_interfaces())
# loop.call_later(30, lambda: task.cancel())

# try:
#     loop.run_until_complete(task)
# except asyncio.CancelledError:
#     pass
