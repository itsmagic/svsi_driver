from websocket import WebSocketApp
import json


class MyApp(object):
    def ws_open(self, ws):
        print("Connected")
        ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'subscribe_instance', "request_id": 42, "pk": 7}}))
        ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'retrieve', "request_id": 42, "pk": 7}}))

    def ws_message(self, ws, message):
        # {"stream": "talentstream", "payload": {"errors": [], "data": null, "action": "subscribe_instance", "response_status": 201, "request_id": 42}}
        # {"stream": "talentstream", "payload": {"errors": [], "data": {"name": "HDMI", "set_on": true, "set_off": false, "is_range": false, "range_value": 0,
        # "requested_range_value": 0, "range_min": 0, "range_max": 100, "state": false, "in_transistion": false, "disable_if_in_transistion": false, "id": 7,
        # "device": 3}, "action": "update", "response_status": 200, "request_id": 42}}
        print('Got message: ', message)
        # talents_actions_dict = {'7': 'set:94'}
        all_data = json.loads(message)
        if all_data['payload']['action'] == 'patch':
            print('It is a patch...')
            if all_data['payload']['errors'] != []:
                print("Patch error: ", all_data['payload']['errors'])
            return
        if all_data['stream'] == "talentstream":
            print('It is a talentstream')
            talent = all_data['payload']['data']
            print('Talent: ', talent)
            if talent['set_on']:
                print('set on')
                # api_queue.put(['api_command', unit, 'set:94'])
                ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'set_on': False}, "request_id": 43, "pk": 7}}))
            if talent['set_off']:
                print('set_off')
                ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'set_off': False}, "request_id": 43, "pk": 7}}))


if __name__ == '__main__':
    app = MyApp()
    url = 'ws://localhost:8000/ws/multi/'
    ws = WebSocketApp(url,
                      header=[f'Authorization: Token 2d5833b2dbe2ca64dcdb397741556bfbb41ff76c'],
                      on_open=lambda ws: app.ws_open(ws),  # <--- This line is changed
                      on_message=lambda ws, msg: app.ws_message(ws, msg),
                      on_error=lambda ws, error: app.ws_error(ws, error),  # Omittable (msg -> error)
                      on_close=lambda ws: app.ws_close(ws))
    # ws = WebSocketApp(url, on_open=app.ws_open, on_message=app.ws_message)
    ws.run_forever()
