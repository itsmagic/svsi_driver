import socket
import re
from websocket import WebSocketApp
from threading import Thread
from pydispatch import dispatcher
from queue import Queue
from pprint import pprint
import json
import datastore


class APIActionsThread(Thread):

    def __init__(self, queue, port=50001):
        Thread.__init__(self)
        self.queue = queue
        self.port = port
        self.file_transfer_complete = False
        self.file_transfer_progress = ''
        self.config_retrieved = False
        self.shutdown = False
        dispatcher.connect(self.unit_upgrading, signal='Unit Upgrading')
        dispatcher.connect(self.unit_progress, signal='HTTP Progress')
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def unit_upgrading(self, sender, data):
        self.file_transfer_complete = True

    def unit_progress(self, sender, data):
        self.file_transfer_progress = data['message']

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def get_status(self, job):
        unit = job[1]

        try:
            unit.status = 'Getting Status'
            dispatcher.send(signal='Status Update', data=unit)
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(b'getStatus')
            data = s.recv(4096)
            if data[:4].decode() != 'SVSI':
                print('not equal: ', data)
                s.close()
                return
            svsi_type = data.decode().split(':')[1].replace('NAME', '')
            reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
            my_dict = {}
            for item in re.findall(reg_pat, data.decode()):
                my_dict[item[0]] = item[1]
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=svsi_type,
                                          # status="broadcast",
                                          get_status=my_dict)
            dispatcher.send(signal="Unit Detected", data=new_unit, multicast=False)
            s.close()
        except Exception as error:
            unit.status = f'Unable to get status: {error}'
            dispatcher.send(signal='Status Update', data=unit)
            print("Error getting status", error)
            return
        unit.status = 'Success'

        dispatcher.send(signal='Status Update', data=unit)

    def api_command(self, job):
        print('in api command')
        unit = job[1]
        command = job[2]
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, self.port))
            s.send(command.encode())
            data = s.recv(4096)
            if data[:5].decode() == 'ERROR':
                unit.status = 'Error'
                dispatcher.send(signal='Status Update', data=unit)
            s.close()
        except Exception as error:
            unit.status = f'Error sending api command {error}'
            dispatcher.send(signal='Status Update', data=unit)
            print(error)

            return
        unit.status = 'Complete'
        dispatcher.send(signal='Status Update', data=unit)

    def ws_open(self, ws):
        print("Connected")
        ws.send(json.dumps({'stream': 'driverconfigstream', 'payload': {"action": 'list', "request_id": 42}}))
        # ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'subscribe_instance', "request_id": 42, "pk": 7}}))
        # ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'retrieve', "request_id": 42, "pk": 7}}))
        # ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'subscribe_instance', "request_id": 42, "pk": 8}}))
        # ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'retrieve', "request_id": 42, "pk": 8}}))
        # unit = datastore.SVSIUnit()
        # unit.ip_address = "172.17.0.159"
        # self.api_command(['api_command', unit, 'set:2412'])

    def ws_message(self, ws, message):
        # {"stream": "talentstream", "payload": {"errors": [], "data": null, "action": "subscribe_instance", "response_status": 201, "request_id": 42}}
        # {"stream": "talentstream", "payload": {"errors": [], "data": {"name": "HDMI", "set_on": true, "set_off": false, "is_range": false, "range_value": 0,
        # "requested_range_value": 0, "range_min": 0, "range_max": 100, "state": false, "in_transistion": false, "disable_if_in_transistion": false, "id": 7,
        # "device": 3}, "action": "update", "response_status": 200, "request_id": 42}}
        print('Got message: ', message)
        all_data = json.loads(message)
        for driverconfig in (all_data['payload']['data']):
            if driverconfig['key'] == 'ip_address':
                unit = datastore.SVSIUnit()
                unit.ip_address = driverconfig['config_value']
                self.api_command(['api_command', unit, 'set:2412'])
        # print(all_data['payload']['data'][0]['config_value'])
        # my_ip = all_data['payload']['data'][0]['config_value']
        # print(type(my_ip))
        # my_json = json.loads(my_ip)
        # print(my_json)
        # print(type(my_json))
        #

        # talents_actions_dict = {'7': 'set:94', '8': 'set:2412'}
        # all_data = json.loads(message)
        # if all_data['payload']['action'] == 'patch':
        #     print('It is a patch...')
        #     if all_data['payload']['errors'] != []:
        #         print("Patch error: ", all_data['payload']['errors'])
        #     return
        # if all_data['stream'] == "talentstream":
        #     print('It is a talentstream')
        #     talent = all_data['payload']['data']
        #     # print('Talent: ', talent)
        #     if talent['set_on']:
        #         print('set on')
        #         # api_queue.put(['api_command', unit, 'set:94'])
        #         if talent['id'] == 7:  # Raspberry Pi
        #             # Clear request
        #             ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'set_on': False, 'state': True}, "request_id": 43, "pk": 7}}))
        #             ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'state': False}, "request_id": 43, "pk": 8}}))
        #             print('at api')
        #             self.api_command(['api_command', unit, talents_actions_dict[str(talent['id'])]])
        #         else:
        #             # Clear request
        #             ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'set_on': False, 'state': True}, "request_id": 43, "pk": 8}}))
        #             ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'state': False}, "request_id": 43, "pk": 7}}))
        #             print('at api')
        #             self.api_command(['api_command', unit, talents_actions_dict[str(talent['id'])]])
        #     if talent['set_off']:
        #         print('set_off')
        #         ws.send(json.dumps({'stream': 'talentstream', 'payload': {"action": 'patch', 'data': {'set_off': False, 'state': False}, "request_id": 43, "pk": talent['id']}}))
        #         self.api_command(['api_command', unit, 'set:0'])


def status(data):
    pprint(data.status)


def main():
    import datastore
    dispatcher.connect(status, signal="Status Update")
    api_queue = Queue()
    api_job_thread = APIActionsThread(api_queue)
    api_job_thread.setDaemon(True)
    api_job_thread.start()
    unit = datastore.SVSIUnit()
    unit.ip_address = "172.17.0.159"
    # api_queue.put(['api_command', unit, 'set:94'])
    # # api_queue.join()

    # Websocket stuff
    url = 'ws://localhost:8000/ws/multi/'
    ws = WebSocketApp(url,
                      header=[f'Authorization: Token 2d5833b2dbe2ca64dcdb397741556bfbb41ff76c'],
                      on_open=lambda ws: api_job_thread.ws_open(ws),  # <--- This line is changed
                      on_message=lambda ws, msg: api_job_thread.ws_message(ws, msg),
                      on_error=lambda ws, error: api_job_thread.ws_error(ws, error),  # Omittable (msg -> error)
                      on_close=lambda ws: api_job_thread.ws_close(ws))
    ws.run_forever()






    api_job_thread.shutdown = True
    api_job_thread.join()


if __name__ == '__main__':
    main()
