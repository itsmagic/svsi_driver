# WS client example

import asyncio
import websockets
import json
import time


async def handler(websocket, my_queue):
    consumer_task = asyncio.ensure_future(
        consumer_handler(websocket))
    producer_task = asyncio.ensure_future(
        producer_handler(websocket, my_queue))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task],
        return_when=asyncio.FIRST_COMPLETED,
    )
    for task in pending:
        task.cancel()


async def consumer_handler(websocket):
    async for message in websocket:
        await consumer(message)


async def producer_handler(websocket, my_queue):
    while True:
        message = await producer(my_queue)
        await websocket.send(message)


async def consumer(message):
    print('In consumer')
    print(message)


async def producer(my_queue):
    while True:
        message = await my_queue.get()
        my_queue.task_done()
        # time.sleep(5)
        print('In producer')
        return message


async def send_lots_o_messages(my_queue):
    while True:
        await asyncio.sleep(10)
        print('adding to queue')
        await my_queue.put(json.dumps({'stream': 'devicestream', 'payload': {"action": "list", "request_id": 42}}))


async def my_connect(my_queue):
    uri = "ws://localhost:8000/ws/multi/"
    ws = await websockets.connect(uri, extra_headers={'Authorization': 'Token d7e51a1e4844d985102fe3d1f61b5ec9466bd1cd'})
    await handler(ws, my_queue)


async def main():
    my_queue = asyncio.Queue()
    # my_ws = asyncio.create_task(my_connect(my_queue))
    # await my_ws
    # my_mess = asyncio.create_task(send_lots_o_messages(my_queue))
    await asyncio.gather(my_connect(my_queue), send_lots_o_messages(my_queue))
    print('after connect')
    # my_queue.put_nowait(json.dumps({'stream': 'devicestream', 'payload': {"action": "list", "request_id": 42}}))
    # uri = "ws://localhost:8000/ws/multi/"
    # ws = await websockets.connect(uri, extra_headers={'Authorization': 'Token d7e51a1e4844d985102fe3d1f61b5ec9466bd1cd'})
    # await handler(ws, my_queue)
    # asyncio.gather(handler(ws, my_queue))  #, send_lots_o_messages(my_queue))

    # asyncio.gather(handler(ws))


# async def hello():
#     
#     async with websockets.connect(uri, extra_headers={'Authorization': 'Token d7e51a1e4844d985102fe3d1f61b5ec9466bd1cd'}) as websocket:
#         # name = input("What's your name? ")

#         await websocket.send((json.dumps({'stream': 'devicestream', 'payload': {"action": "list", "request_id": 42}})))
#         # print(f"> {name}")

#         greeting = await websocket.recv()
#         print(f"< {greeting}")




asyncio.run(main())