from reconnecting_websocket import ReconnectingWebsocket
from svsi_scanner import SVSIScanner
from svsi_monitor import SVSIMonitor
from interface_monitor import InterfaceMonitor
from api_actions import APIActionsThread
from main_list_queue import MainListQueue
from svsi_unit_monitor import SVSIUnitMonitor
import time
from threading import Thread, Timer
from pydispatch import dispatcher
import pickle
import json
import datastore
from pathlib import Path
import os
import socket
import re
import queue



class SVSiDriver(Thread):
    """The SVSi Driver Thread"""

    def __init__(self, conf=None, current_units=[]):
        """Init the worker class"""
        self.shutdown = False
        self.conf = conf
        self.ws_connected = False
        self.authorized = False
        self.current_units = []
        self.main_list = []
        self.multicast_listeners = []
        self.interface_list = []
        self.devices_to_be_added = []
        self.monitor_threads = {}

        dispatcher.connect(self.shutdown_signal, signal="Shutdown")
        dispatcher.connect(self.ws_incoming, signal="Websocket Incoming")
        dispatcher.connect(self.ws_status, signal="Websocket Status")
        dispatcher.connect(self.discovered_new_unit, signal="Unit Detected")
        dispatcher.connect(self.interface_update, signal="Interface Update")

        self.interface_queue = queue.Queue()
        ip_monitor = InterfaceMonitor(self.interface_queue)
        ip_monitor.setDaemon(True)
        ip_monitor.start()

        self.main_list_queue_thread = queue.Queue()
        update_queue = MainListQueue(self.main_list, self.main_list_queue_thread)
        update_queue.setDaemon(True)
        update_queue.start()

        self.api_queue = queue.Queue()
        for i in range(20):
            api_thread = APIActionsThread(self.api_queue, self)
            api_thread.setDaemon(True)
            api_thread.start()

        self.ws = Thread(target=ReconnectingWebsocket, kwargs={'token': self.conf.get_token()})
        self.ws.daemon = True

        self.interface_queue.put(['check_changes'])

        self.stage = 0
        self.stages = ['register', 'verify uuid', 'verify authorization', 'adding devices', 'running']
        Thread.__init__(self)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def restart_multicast(self):
        """We need a thread to listen on every interface"""
        print('restarting multicast')
        for listener in self.multicast_listeners:
            # print('stopping multicast')
            listener.shutdown = True
            listener.join()
        for interface in self.interface_list:
            multicast_listener = SVSIMonitor(ip_address=interface.ip_addresses[0])
            multicast_listener.setDaemon(True)
            multicast_listener.start()
            self.multicast_listeners.append(multicast_listener)

    def on_broadcast(self):
        my_interfaces = []
        for interface in self.interface_list:
            print('sending broadcast on interface: ', interface)
            my_scan = SVSIScanner(interface=interface)
            my_interfaces.append(interface.description)
            my_scan.setDaemon(True)
            my_scan.start()

        names = "\r".join(my_interfaces)
        print(f'Broadcast search has been sent via:\r\r{names}')

    def interface_update(self, sender, interface_list):
        """Processes updates"""
        print('In interface update')
        self.interface_list = interface_list
        print('Starting multicast and sending broadcast')
        self.restart_multicast()
        self.on_broadcast()

    def ws_incoming(self, message):
        print("incoming: ", message)
        # {"stream": "driverstream",
        #   "payload": {"errors": [],
        #               "data": {"name": "NewSVSiDriver", "owner": "driver", "is_online": true,
        #               "authorized": false, "id": "a34d2d2a-6269-43af-b484-625950d4a66b", "devices": [], "configs": []},
        #                "action": "create", "response_status": 201, "request_id": 42}}
        all_data = json.loads(message)
        if all_data['stream'] == "driverstream":
            print('driverstream')
            driver = all_data['payload']['data']
            # Created
            print('driver: ', driver)
            if all_data['payload']['action'] == "create":
                # need to store uuid
                print('store uuid')
                self.conf.uuid = driver['driver_uuid']
                self.conf.driver_id = int(driver['id'])
                self.save_conf()
                self.stage = 1
            if all_data['payload']['action'] == "retrieve":
                if driver['driver_uuid'] == self.conf.uuid:
                    # we have a match continue
                    if not driver['authorized']:
                        self.stage = 2
                    else:
                        self.stage = 3
            # Authorized
            if driver['authorized']:
                print('authorized')
                self.authorized = True

        if all_data['stream'] == "devicestream":
            if all_data['payload']['action'] == "list":
                # Check all devices are registered
                print('found list')
                print('mainlist: ', len(self.main_list))
                print('newlist: ', len(all_data['payload']['data']))

                self.devices_to_be_added = [i for i in self.main_list if i.mac_address not in [j['unique_id'] for j in all_data['payload']['data']]]
                # for svsi_unit in self.main_list:
                #     # for device in all_data['payload']['data']:

                #     #     # if device['driver']['owner'] == 'driver':  # will need to fix this
                #     #     #     print('found one of my devices: ')
                #     #     #     if device['unique_id'] == svsi_unit.mac_address
                #     #     # else:
                #     #     #     print('found someone elses devices')

        # if all_data['stream'] == "videoroutestream":
        #     videoroute = all_data['payload']['data']:
        #         # if this route doesn't exists
        #     self.ws_send(stream='talentstream', payload={'action': 'create',
        #                                                  'data': {'name': f'Select input 2',
        #                                                           'device': 13},
        #                                                  'request_id': 42})

    def ws_status(self, status):
        print("ws status: ", status)
        if status['connected']:
            print('setting connected to True')
            self.ws_connected = True
        else:
            print('setting connected to False')
            self.ws_connected = False

    def discovered_new_unit(self, sender, data, multicast):
        """Unit discovered by multicast"""
        self.main_list_queue_thread.put(['add_obj', data, multicast])

    def get_svsi_unit_status(self, unit):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((unit.ip_address, 50001))
            s.send(b'getStatus')
            data = s.recv(4096)
            if data[:4].decode() != 'SVSI':
                print('not equal: ', data)
                s.close()
                return
            svsi_type = data.decode().split(':')[1].replace('NAME', '')
            reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
            my_dict = {}
            for item in re.findall(reg_pat, data.decode()):
                my_dict[item[0]] = item[1]
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=svsi_type,
                                          # status="broadcast",
                                          get_status=my_dict)
            s.close()
            return new_unit
        except Exception as error:
            print("Error getting status", error)
            return unit

    def run(self):
        """Run Worker Thread."""

        self.ws.start()
        self.on_broadcast()
        requested_uuid = False
        while not self.shutdown:

            if self.stage == 0:

                if not self.ws_connected:
                    time.sleep(1)
                    print('connecting...')
                    print(self.main_list)
                    continue
                print('Stage: ', self.stages[self.stage])
                if self.conf.uuid:
                    print('We have a uuid: ', self.conf.uuid)
                    self.stage = 1
                    continue
                else:
                    if not requested_uuid:
                        # We need to get a UUID
                        print('Requesting UUID')
                        requested_uuid = True
                        self.ws_send(stream='driverstream', payload={'action': 'create',
                                                                     'data': {'owner': {'id': self.conf.user_id},
                                                                              'name': 'NewSVSiDriver',
                                                                              'devices': [],
                                                                              'configs': []},
                                                                     'request_id': 42})
                time.sleep(1)
                continue
            if self.stage == 1:
                print('Stage: ', self.stages[self.stage])
                print('Verifying UUID')
                # Try to get our driver to ensure our UUID is correct
                print('driver_id: ', self.conf.driver_id)
                self.ws_send(stream='driverstream', payload={"action": "retrieve", "request_id": 42, "pk": self.conf.driver_id})
                time.sleep(1)
                continue
            if self.stage == 2:
                print('Stage: ', self.stages[self.stage])
                print('Waiting for Authorized')
                self.ws_send(stream='driverstream', payload={"action": "retrieve", "request_id": 42, "pk": self.conf.driver_id})
                time.sleep(15)  # Longer wait here as we would skip this step if it was done, we need the admin to log in and give authorized.
                continue
            if self.stage == 3:  # Monitoring
                print('Stage: ', self.stages[self.stage])

                # We need to do the following repeatedly
                ## Check all svsi devices are in the system
                ## If the list of devices hasn't changed we just wait


                self.ws_send(stream='devicestream', payload={"action": "list", "request_id": 42})
                for svsi_unit in self.devices_to_be_added:
                    print('creating: ', svsi_unit.name)
                    self.ws_send(stream='devicestream', payload={'action': 'create', 'data': {'name': svsi_unit.name, 'unique_id': svsi_unit.mac_address}, 'driver': self.conf.uuid, "request_id": 42})
                self.devices_to_be_added = []
                for unit in self.main_list:
                    if unit not in self.monitor_threads:
                        # Start thread to monitor unit
                        my_queue = queue.Queue()
                        my_monitor = SVSIUnitMonitor(unit=unit, job_queue=my_queue)
                        my_monitor.daemon = True
                        my_monitor.start()
                        self.monitor_threads[unit] = (my_monitor, my_queue)

                # print('Update units')
                # At this point we are registered and authorized to add devices/ talents
                # print('Current units: ', self.main_list)

                # new_units = []
                # if not checked_units:
                #     checked_units = True
                #     # for unit in self.current_units:
                #     #     # Check if it is a enc or dec
                #     #     new_units.append(self.get_svsi_unit_status(unit))
                #     # self.conf.svsi_devices = new_units
                #     # self.save_conf()
                #     # for unit in self.current_units:
                #     #     if unit.box_type == 'TX':
                #     # self.ws_send(stream='devicestream', payload={'action': 'create',
                #     #                                              'data': {'name': f'N2400 -- 172.17.0.159 -- 00:60:90:00:01:00',
                #     #                                                       'is_online': True,
                #     #                                                       'driver': self.conf.driver_id,
                #     #                                                       'talents': []},
                #     #                                              'request_id': 42})
                #     # self.ws_send(stream='devicestream', payload={'action': 'create',
                #     #                                              'data': {'name': f'N2400 -- 172.17.0.2 -- 00:60:90:00:01:22',
                #     #                                                       'is_online': True,
                #     #                                                       'driver': self.conf.driver_id,
                #     #                                                       'talents': []},
                #     #                                              'request_id': 42})
                #     # self.ws_send(stream='videoinputstream', payload={'action': 'create',
                #     #                                                  'data': {'name': f'Input -- 172.17.0.159 -- 00:60:90:00:01:00',
                #     #                                                           'number': 12,
                #     #                                                           'device': 12,
                #     #                                                           'videoroutes': []},
                #     #                                                  'request_id': 42})
                #     # self.ws_send(stream='videooutputstream', payload={'action': 'create',
                #     #                                                   'data': {'name': f'Output -- 172.17.0.159 -- 00:60:90:00:01:00',
                #     #                                                            'number': 2412,
                #     #                                                            'device': 13,
                #     #                                                            # 'driver': self.conf.driver_id,
                #     #                                                            # 'talents': [],
                #     #                                                            'videoroutes': []
                #     #                                                            },
                #     #                                                   'request_id': 42})
                #     # self.ws_send(stream='videoroutestream', payload={"action": "list", "request_id": 42})
                #     time.sleep(5)
                #     quit()

                time.sleep(5)
                # quit()
                continue

            time.sleep(5)
            quit()

    def ws_send(self, stream, payload):
        if self.ws_connected:
            dispatcher.send("Websocket Send", stream=stream, payload=payload)
        else:
            print('Unable to send when disconnected')

    def save_conf(self):
        with open(os.path.join(Path.cwd(), 'SVSiDriver.conf'), 'wb') as f:
            self.conf.svsi_devices = self.current_units
            pickle.dump(self.conf, f)
            print('saved conf')


def main():
    try:
        with open(os.path.join(Path.cwd(), 'SVSiDriver.conf'), 'rb') as f:
            my_conf = pickle.load(f)
    except Exception as error:
        print("unable to load conf: ", error)
        my_conf = datastore.Config()
        with open(os.path.join(Path.cwd(), 'SVSiDriver.conf'), 'wb') as f:
            pickle.dump(my_conf, f)

    my_conf.set_token('d7e51a1e4844d985102fe3d1f61b5ec9466bd1cd')
    my_conf.user_id = 6
    my_driver = SVSiDriver(conf=my_conf)
    my_driver.daemon = True
    my_driver.start()
    my_driver.join()


if __name__ == '__main__':
    main()

# # Start UP # #
# check if we have a uuid
# # if not register as a driver
# # store our uuid
# retrieve existing devices


# # Thread # #
# add new devices as discovered


# if we are authorized
# # register new inputs and outputs
# # inputs will have on and off and number (stream number for svsi, port number for a switch)
# # outputs will have on and off

# # Thread # #
# monitor for new routes
# if a new route is created
# # check if it is ours ? // maybe filter on device
# # create talnet for route
# if a route is deleted
# # We need to delete the Talent

# # Thread # #
# monitor for talent on/off requests
# control devices as necessary
