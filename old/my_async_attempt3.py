import sys
import asyncio
import logging
import wmi
import datastore
import socket
import binascii
import re
import datetime
import websockets
import json


INTERFACES = []
DISCOVERED_SVSI = []
SVSI_UNIT_QUEUES = {}
ws_send_queue = asyncio.Queue()

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("my_test_prog")
logging.getLogger("chardet.charsetprober").disabled = True


async def broadcast_scan_for_svsi():
    while True:
        data_packs = ['0114', '0119', '0196', '0198', '01b4', '01b5', '01b6',
                      '01b7', '01b8', '01b9', '01ba', '01ca', '01cb', '01cc',
                      '01cd', '01ce']
        data_tail = 'cafe1234ffffffff'
        for interface in get_current_interfaces():
            logger.info(interface)
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                sock.bind((interface.ip_addresses[0], 0))
                sock.setblocking(0)
            except IOError:
                # something has already used the port, its probably not us because
                # multiple instances can run without error due to SO_REUSEADDR
                logger.error('Socket in use')
                continue
        try:
            for data in data_packs:
                
                if data == '01b7':
                    logger.error('sendings 01b7 packet')
                    sock.sendto(binascii.unhexlify(data + data_tail), ("255.255.255.255", 50006))
                else:
                    logger.error('sendings else packet')
                    sock.sendto(binascii.unhexlify(data + data_tail), ("255.255.255.255", 50001))
                await asyncio.sleep(.1)
        except IOError as error:
            if error.args[0] == 10051:
                logger.error(error)
            else:
                logger.error(error)
                # return?
        data = ""
        count = 0
        timeout = 15
        waiting = True
        while waiting:
            count += 1
            if count > timeout:
                break
            try:
                # print('waiting')
                data, source = sock.recvfrom(4000)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    await asyncio.sleep(1)
                    continue
                else:
                    # Connection was probably closed
                    break
                    # print("Error listening: ", error)
            try:
                if data == '' or data[:4].decode() != 'SVSI':
                    # print('not equal: ', data)
                    continue
                serial_number = data.decode().split(':')[1].replace('NAME', '')
                reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                my_dict = {}
                for item in re.findall(reg_pat, data.decode()):
                    my_dict[item[0]] = item[1]
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=serial_number,
                                          # status="broadcast",
                                          get_status=my_dict)
            new_unit.get_status_populate()
            logger.error(new_unit)
            # Found units we need to create a queue and connect to them
            if new_unit not in DISCOVERED_SVSI:
                logger.error(f'Found new unit {new_unit.mac_address}')
                DISCOVERED_SVSI.append(new_unit)
                await process_new_unit()
            # else:
            #     # logger.error(f'Found old unit {new_unit.mac_address}')
        logger.error('Scan complete')
        await asyncio.sleep(10)


async def multicast_monitor_launcher():
    my_ips = []
    for interface in get_current_interfaces():
        my_ips.append(interface.ip_addresses[0])

    await asyncio.gather(*(multicast_monitor_for_svsi(ip) for ip in my_ips))


async def multicast_monitor_for_svsi(ip_address):
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            port = 50019
            multicast_address = "239.254.12.16"
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((ip_address, port))
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(ip_address))

            sock.setblocking(0)

        except IOError as error:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            if error.args[0] == 10049:
                print("Skipping this error: ", error)
            else:
                print('Opening socket error: ', error)
                # Maybe we should warn the user?
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        data = ""
        while True:
            try:
                # print('waiting')
                data, source = sock.recvfrom(1024)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    await asyncio.sleep(1)
                    continue
                else:
                    print("Error listening: ", error)
            # check if it is a discovery message
            try:
                if data[:14].decode() != 'SVSI-Discovery':
                    print('not equal: ', data[:14].decode())
                    continue
                mac_address = ':'.join(['%02x' % item for item in data[16:22]])
                unit = datastore.SVSIUnit(arrival_time=datetime.datetime.now(),
                                          serial_number=data[30:44].decode(),
                                          name=data[50:67].decode(),
                                          mac_address=mac_address,
                                          ip_address=source[0],
                                          raw_multicast=data)
                logger.error(unit)
            except Exception as error:
                print('Error parsing packet: ', error)
                continue
            # dispatcher.send(signal="Unit Detected", sender=self, data=unit, multicast=True)
    except asyncio.CancelledError:
        logger.error('In cancelled error')
        # try:
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(ip_address))
        # print('leave: ', self.ip_address)
        sock.close()
        # print('socket closed')
        # 
        raise
    finally:
        logger.error('After cancelled')


async def connect_to_unit(unit, q):
    logger.error(f"Getting unit status {unit.mac_address}")
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((unit.ip_address, 50001))
        s.send(b'getStatus')
        data = s.recv(4096)
        if data[:4].decode() != 'SVSI':
            print('not equal: ', data)
            s.close()
            return
        serial_number = data.decode().split(':')[1].replace('NAME', '')
        reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
        my_dict = {}
        for item in re.findall(reg_pat, data.decode()):
            my_dict[item[0]] = item[1]
        unit.serial_number = serial_number
        unit.get_status = my_dict
        unit.get_status_populate()
        logger.error(f"Got unit status{unit.name}")
        while True:
            action = await q.get()
            logger.error('Got action')
            logger.error(action)


        # s.close()
    except Exception as error:
        logger.error(f"Getting unit status error {error}")
        return


def get_current_interfaces():
    interfaces = []
    # logger.error('starting update update_current_interfaces')
    for interface in wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True):
        new_interface = datastore.InterfaceConfig(index=interface.Index,
                                                  description=interface.Description,
                                                  ip_addresses=list(interface.IPAddress),
                                                  subnet_addresses=list(interface.IPSubnet),
                                                  gateway_addresses=interface.DefaultIPGateway,
                                                  dhcp_enabled=interface.DHCPEnabled)

        new_interface.populate_broadcast_addresses()
        interfaces.append(new_interface)
    return interfaces
    # await asyncio.sleep(5)


async def process_new_unit():
    # pass
    logger.error(f'In process_new_units')
    logger.error(DISCOVERED_SVSI)


#  ****** WS Stuff ****** #
async def handler(websocket, ws_send_queue):
    consumer_task = asyncio.ensure_future(
        consumer_handler(websocket))
    producer_task = asyncio.ensure_future(
        producer_handler(websocket, ws_send_queue))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task],
        return_when=asyncio.FIRST_COMPLETED,
    )
    for task in pending:
        task.cancel()


async def consumer_handler(websocket):
    async for message in websocket:
        await consumer(message)


async def producer_handler(websocket, ws_send_queue):
    while True:
        message = await producer(ws_send_queue)
        await websocket.send(message)


async def consumer(message):
    print('In consumer')
    print(message)


async def producer(ws_send_queue):
    while True:
        message = await ws_send_queue.get()
        ws_send_queue.task_done()
        # time.sleep(5)
        print('In producer')
        return message


async def ws_connect(ws_send_queue):
    uri = "ws://localhost:8000/ws/multi/"
    ws = await websockets.connect(uri, extra_headers={'Authorization': 'Token d7e51a1e4844d985102fe3d1f61b5ec9466bd1cd'})
    await handler(ws, ws_send_queue)


async def main():
    ws_send_queue = asyncio.Queue()
    my_uuid = None
    my_id = 7
    try:
        # await asyncio.gather(ws_connect(ws_send_queue))
        await asyncio.gather(broadcast_scan_for_svsi(), return_exceptions=True)
    except asyncio.CancelledError:
        print("Gracefully shutting down main")
        raise
    finally:
        print('Shutdown main')


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        pass
    finally:
        print('Shutdown')

