
import socket
import asyncio
import binascii

from string import ascii_letters
import random
from typing import Tuple, Union, Text

Address = Tuple[str, int]

class BroadcastProtocol(asyncio.DatagramProtocol):

    def __init__(self, target: Address, *, loop: asyncio.AbstractEventLoop = None):
        self.target = target
        self.loop = asyncio.get_event_loop() if loop is None else loop

    def connection_made(self, transport: asyncio.transports.DatagramTransport):
        print('started')
        self.transport = transport
        sock = transport.get_extra_info("socket")  # type: socket.socket
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.broadcast()

    def datagram_received(self, data: Union[bytes, Text], addr: Address):
        print('data received:', data, addr)

    def broadcast(self):
        data_packs = ['0114', '0119', '0196', '0198', '01b4', '01b5', '01b6',
                      '01b7', '01b8', '01b9', '01ba', '01ca', '01cb', '01cc',
                      '01cd', '01ce']
        data_tail = 'cafe1234ffffffff'
        # string = ''.join([random.choice(ascii_letters) for _ in range(5)])
        # print('sending:', string)
        for data in data_packs:
            self.transport.sendto(binascii.unhexlify(data + data_tail), self.target)
        # self.loop.call_later(5, self.broadcast)

loop = asyncio.get_event_loop()
coro = loop.create_datagram_endpoint(
    lambda: BroadcastProtocol(('255.255.255.255', 50001), loop=loop), local_addr=('10.0.0.113', 50001))
loop.run_until_complete(coro)
loop.run_forever()
loop.close()