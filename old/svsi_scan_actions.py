from threading import Thread
import time
from . import svsi_scanner
from pydispatch import dispatcher


class SVSIScanActionsThread(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        self.active_scans = 0
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            job = self.queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def scan(self, job):
        # while(threading.activeCount() > 100):
        #     time.sleep(1)
        interface = job[1]
        ip_address = job[2]
        # my_scan = svsi_scanner.SVSIScanner(interface=interface, ip_address=ip_address)
        # # print('starting job')
        # # dispatcher.send(signal="Scan Update", data={'completed': 0, 'started': 1})
        # my_scan.run()
        # # dispatcher.send(signal="Scan Update", data={'completed': 1, 'started': 0})
        # # print('finished job')

        thread_started = False
        while not thread_started:
            # and not self.dlg.WasCancelled():
            try:
                test = svsi_scanner.SVSIScanner(interface, ip_address=ip_address)
                test.setDaemon(True)
                test.start()
                thread_started = True
            except RuntimeError:
                # We have run out of threads, wait fo some to finish
                # for i in range(15):
                # if not self.dlg.Update(self.scans_started, f"{self.scans_started} out of {self.dlg.GetRange()} scans started"):
                #     self.dlg.Destroy()
                #     return
                time.sleep(1)
                pass
