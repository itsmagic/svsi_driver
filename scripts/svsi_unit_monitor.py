import socket
import time
# import datetime
from threading import Thread, Timer
from pydispatch import dispatcher
# import datastore
import re
import queue
import sys
import logging

logger = logging.getLogger("SVSIUnitMonitor")

class SVSiUnitMonitor(Thread):
    """The SVSi thread"""

    def __init__(self, unit, job_queue, port=50002):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.unit = unit
        self.port = port
        self.current_stream = 0
        self.job_queue = job_queue

        # Monitored items
        self.current_stream = 0
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        Thread.__init__(self, name=unit.mac_address)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def connect(self):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(2)
            s.connect((self.unit.ip_address, self.port))
            self.unit.online = True
            if not self.shutdown:
                dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'online': True})
        except Exception as error:
            logger.error(f"Error connecting to unit {self.unit.mac_address}: {error}")

            # if not self.shutdown:
            #     # Error 10051 is ip unreachable 
            #     dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'checkip': True})
            if self.unit.online:
                if not self.shutdown:
                    dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'online': False})
            self.unit.online = False
            # time.sleep(15)
            return None
        return s

    def unit_status_timer(self):
        if self.s is None:
            self.s = self.connect()
            time.sleep(5)
            self.unit_status_timer()

        else:
            self.job_queue.put(['get_status'])
            self.unit_refresh = Timer(5, self.unit_status_timer)
            self.unit_refresh.start()

    def run(self):
        self.s = self.connect()
        if self.s is None:
            time.sleep(15)
            self.run()
        self.unit_refresh = Timer(5, self.unit_status_timer)
        self.unit_refresh.start()
        while not self.shutdown:
            # gets the job from the queue
            job = self.job_queue.get()
            getattr(self, job[0])(job)

            # send a signal to the queue that the job is done
            self.job_queue.task_done()

    def get_status(self, job):
        try:
            if self.s is None:
                return
            self.s.send(b'getStatus')
            data = self.s.recv(4096)
            # if data[:4].decode() != 'SVSI':
            #     logger.info('not equal: ', data)
            #     self.s.close()
            #     return
            svsi_type = data.decode().split(':')[1].replace('NAME', '')
            reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
            my_dict = {}
            for item in re.findall(reg_pat, data.decode()):
                my_dict[item[0]] = item[1]
            self.unit.get_status = my_dict
            self.unit.serial_number = svsi_type
            # plogger.info(self.unit.__dict__)
            self.unit.get_status_populate()
            if not self.shutdown:
                # dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit})
                if str(self.current_stream) != str(self.unit.stream):
                    # Stream has been updated
                    dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'stream': self.unit.stream})
                    self.current_stream = self.unit.stream
        except Exception as error:
            if not self.shutdown:
                logger.info(f"Unable to get status: {error} from {self.unit.mac_address}")
                if self.unit.online:
                    dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'online': False})
                self.unit.online = False
                self.s = None

    def change_stream(self, job):
        stream = job[1]
        try:
            if self.s is None:
                return
            self.s.send(f'set:{stream}\r'.encode())
            if not self.shutdown:
                dispatcher.send(signal="SVSI Unit Status", data={'unit': self.unit, 'stream': stream})
            self.current_stream = stream
        except Exception as error:
            logger.info(f"error change stream {error}")

    def command(self, job):
        command = job[1]
        try:
            if self.s is None:
                return

            self.s.send(f'{command}\r'.encode())
        except Exception as error:
            logger.info(f"error change stream {error}")

def status(data):
    logger.info(data)


def main():
    dispatcher.connect(status, signal="SVSI Unit Status")
    import datastore
    my_unit = datastore.SVSIUnit(ip_address='172.17.0.77')
    my_queue = queue.Queue()
    my_thread = SVSiUnitMonitor(unit=my_unit, job_queue=my_queue)
    my_thread.daemon = True
    my_thread.start()
    time.sleep(20)


if __name__ == '__main__':
    main()
