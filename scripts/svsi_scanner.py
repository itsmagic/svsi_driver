import socket
import binascii
import time
# import ipaddress

from threading import Thread
from pydispatch import dispatcher
import re
from scripts import datastore
import sys
import logging

logger = logging.getLogger("SVSIScanner")

class SVSiScanner(Thread):
    """The SVSi thread"""

    def __init__(self, interface, ip_address='255.255.255.255', timeout=15):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.interface = interface
        self.ip_address = ip_address
        self.timeout = timeout
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown Scan",
                           sender=dispatcher.Any)
        Thread.__init__(self, name="SVSiScannerThread")

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        """Run Worker Thread."""
        data_packs = ['0114cafe1234ffffffff',

                      '0119cafe1234ffffffff',

                      '0196cafe1234ffffffff',

                      '0198cafe1234ffffffff',

                      '01b4cafe1234ffffffff',
                      '01b5cafe1234ffffffff',
                      '01b6cafe1234ffffffff',
                      '01b7cafe1234ffffffff',
                      '01b8cafe1234ffffffff',
                      '01b9cafe1234ffffffff',
                      '01bacafe1234ffffffff',

                      '01cacafe1234ffffffff',
                      '01cbcafe1234ffffffff',
                      '01cccafe1234ffffffff',
                      '01cdcafe1234ffffffff',
                      '01cecafe1234ffffffff']

        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            if self.ip_address == "255.255.255.255":
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            sock.bind((self.interface.ip_addresses[0], 0))
            sock.setblocking(0)

        except IOError:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            dispatcher.send(signal="SVSIMonitor", sender=self, data={'Error': 'Socket in use'})
            return
        try:
            for data in data_packs:
                if data == '01b7cafe1234ffffffff':
                    sock.sendto(binascii.unhexlify(data), (self.ip_address, 50006))
                else:
                    sock.sendto(binascii.unhexlify(data), (self.ip_address, 50001))
                time.sleep(.1)
        except IOError as error:
            if error.args[0] == 10051:
                logger.info(f"Skipping this error: {error}")
            else:
                logger.info(f"Sendto socket error: {error}")
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        # for data in data_pack_2:
        #     sock.sendto(binascii.unhexlify(data), ('255.255.255.255', 50001))
        #     # time.sleep(.02)
        dispatcher.send(signal="Scan Update", data={'completed': 0, 'started': 1})
        data = ""
        count = 0
        while not self.shutdown:
            count += 1
            if count > self.timeout:
                break
            try:
                # logger.info('waiting')
                data, source = sock.recvfrom(4000)
            except Exception as error:
                if error.args[0] == 10035:
                    # no data
                    # lets wait a bit and check again
                    time.sleep(1)
                    continue
                else:
                    # Connection was probably closed
                    break
                    # logger.info("Error listening: ", error)
            try:
                if data == '' or data[:4].decode() != 'SVSI':
                    # logger.info('not equal: ', data)
                    continue
                svsi_type = data.decode().split(':')[1].replace('NAME', '')
                reg_pat = re.compile("(.*?):(.*?)(\\r|$)")
                my_dict = {}
                for item in re.findall(reg_pat, data.decode()):
                    my_dict[item[0]] = item[1]
            except Exception as error:
                logger.info(f"Error parsing packet: {error}")
                continue
            new_unit = datastore.SVSIUnit(ip_address=my_dict["IP"],
                                          mac_address=my_dict["MAC"].lower(),
                                          serial_number=svsi_type,
                                          # status="broadcast",
                                          get_status=my_dict)
            dispatcher.send(signal="Unit Detected", data=new_unit, multicast=False)

        if not self.shutdown:
            dispatcher.send(signal="Scan Update", data={'completed': 1, 'started': 0})


def incoming(sender, data):
    logger.info(sender)
    logger.info(data)


def new_unit(sender, data, multicast):
    logger.info(data)
    logger.info(multicast)


def main():
    dispatcher.connect(incoming, signal="SVSIMonitor", sender=dispatcher.Any)
    dispatcher.connect(new_unit, signal="Unit Detected")
    import datastore
    my_interface = datastore.InterfaceConfig()
    my_interface.ip_addresses = ['10.0.0.113']
    my_interfaces = [my_interface]
    # import pickle
    # with open('interface.pkl', 'rb') as f:
    #     my_interfaces = pickle.load(f)

    # Broadcast scan
    for interface in my_interfaces:
        test = SVSIScanner(interface)
        test.setDaemon(True)
        test.start()

    time.sleep(10)
    # test.shutdown = True


if __name__ == '__main__':
    import datastore
    main()
