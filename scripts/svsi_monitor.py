import socket
import time
import datetime
from threading import Thread, local
from pydispatch import dispatcher
from scripts import datastore
import sys
import logging


logger = logging.getLogger("SVSIMonitor")

class SVSiMonitor(Thread):
    """The SVSi thread"""

    def __init__(self, ip_address):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.ip_address = ip_address
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        Thread.__init__(self, name="SVSIMonitorThread")

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        """Run Worker Thread."""
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            port = 50019
            multicast_address = "239.254.12.16"
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((self.ip_address, port))
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(self.ip_address))

            sock.setblocking(0)

        except IOError as error:
            # something has already used the port, its probably not us because
            # multiple instances can run without error due to SO_REUSEADDR
            if error.args[0] == 10049:
                logger.info(f"Skipping this error: {error}")
            else:
                logger.critical(f"Opening socket error: {error}")
                # Maybe we should warn the user?
                # dispatcher.send(signal="Error Message", sender=self, data={'message': 'Another program is using the SVSI multicast port.\rPlease check if N-Able is running,\ror use broadcast to discover units.', 'caption': 'Socket in use'})
            return
        data = b""
        while not self.shutdown:
            try:
                # logger.info('waiting')
                data, source = sock.recvfrom(1024)
            except Exception as error:
                if error.args[0] == 10035 or error.args[0] == 11:
                    # no data
                    # lets wait a bit and check again
                    time.sleep(1)
                    continue
                else:
                    time.sleep(5)
                    logger.info(f"Error listening: {error}")
                    continue
            # check if it is a discovery message
            try:
                if data[:14].decode() != 'SVSI-Discovery':
                    logger.info(f"not equal: {data[:14].decode()}")
                    continue
                mac_address = ':'.join(['%02x' % item for item in data[16:22]])
                unit = datastore.SVSIUnit(arrival_time=datetime.datetime.now(),
                                          serial_number=data[30:44].decode(),
                                          name='',  # This wasn't working data[50:67].decode(),
                                          mac_address=mac_address,
                                          ip_address=source[0],
                                          raw_multicast=data)
                # logger.info(unit)
            except Exception as error:
                logger.info(f"Error parsing packet: {error}")
                continue
            # logger.info('Found a unit via multicast')
            dispatcher.send(signal="Unit Detected", sender=self, data=unit, multicast=True)
        try:
            sock.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, socket.inet_aton(multicast_address) + socket.inet_aton(self.ip_address))
            # logger.info('leave: ', self.ip_address)
            sock.close()
            # logger.info('socket closed')
        except Exception as error:
            logger.info(f"Unable to close socket: {error}")


def incoming(data):
    logger.info(data)


def new_unit(data):
    logger.info(data)


def main():

    dispatcher.connect(incoming, signal="Error Message")
    dispatcher.connect(new_unit, signal="Unit Detected")
    test = SVSiMonitor()
    test.setDaemon(True)
    test.start()
    import time
    time.sleep(30)
    test.shutdown = True
    # test.join()


if __name__ == '__main__':
    main()
