from threading import Thread
import sys
import logging

logger = logging.getLogger("SVSIDevicesQueue")

class SVSiDevicesQueue(Thread):
    """The ip monitor thread"""

    def __init__(self, main_list, my_queue):
        """Init Worker Thread Class."""
        self.shutdown = False
        self.main_list = main_list
        self.my_queue = my_queue
        Thread.__init__(self, name="SVSiDevicesQueueThread")

    def run(self):
        """Run Worker Thread."""
        while True:
            # gets the job from the queue
            job = self.my_queue.get()
            # print 'job: ', job
            getattr(self, job[0])(job)
            # send a signal to the queue that the job is done
            self.my_queue.task_done()

    def add_obj(self, command):
        logger.info(f"Adding object before: {self.main_list}")
        duplicate_list = []
        data = command[1]
        multicast = command[2]

        for obj in self.main_list:
            if obj.mac_address == data.mac_address:
                # print('duplicate')
                duplicate_list.append(obj)

        # Add or update list
        if duplicate_list != []:
            # remove duplicates
            if len(duplicate_list) > 1:
                for item in duplicate_list[1:]:
                    self.main_list.pop(item)
            # update duplicate with new info for multicast
            obj = duplicate_list[0]
            obj.ip_address = data.ip_address
            obj.arrival_time = data.arrival_time
            if not multicast:
                # Add the get_status
                obj.get_status = data.get_status
        else:
            # add object
            # if multicast:
            #     data.status = "multicast"
            # else:
            #     data.status = "broadcast"
            self.main_list.append(data)
            obj = data
        obj.get_status_populate()
        logger.info(f"Adding object after: {self.main_list}")



